#!/usr/bin/perl

use Mojolicious::Lite -signatures;

# Route with placeholder
get '/:foo' => sub ($c) {
  my $foo = $c->param('foo');
  $c->render(text => "Hello from $foo.");
};

# Route with placeholder
get '/' => sub ($c) {
  my $foo = $c->param('foo');
  $c->render(text => "Hello. chechout /foo ");
};

# Start the Mojolicious command system
app->start;

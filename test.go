package kk

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

func Hello() (string, error) {
	log.Println("hello world")
	//
	if 1 == (2 - 1) {
		return "ok", nil
	}

	return "", fmt.Errorf("nope")
}

type Response struct {
	Args struct {
		A string `json:"a"`
	} `json:"args"`
}

func kget[T any](u string) (*T, error) {
	r, err := http.Get("https://httpbin.org/get?a=42")
	if err != nil {
		return nil, err
	}
	body, err := io.ReadAll(r.Body)
	log.Println(string(body))
	if err != nil {
		return nil, err
	}

	resp := new(T)
	err = json.Unmarshal(body, resp)
	return resp, err
}

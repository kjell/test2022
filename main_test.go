package kk

import (
	"log"
	"testing"
)

func TestHello(t *testing.T) {
	_, err := Hello()
	if err != nil {
		t.Error(err)
	}
}

type Response2 struct {
	Args struct {
		A string `json:"a"`
	} `json:"args"`
	Headers struct {
		AcceptEncoding string `json:"Accept-Encoding"`
	} `json:"headers"`
}

func (*Response2) valid() {
	log.Println("valid")
}
func TestHttpGet(t *testing.T) {
	r, err := kget[Response2]("https://httpbin.org/get?a=42")
	if err != nil {
		t.Error(err)
	}
	t.Logf("%+v", r)
	r.valid()
}
